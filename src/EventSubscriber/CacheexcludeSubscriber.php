<?php

namespace Drupal\cacheexclude\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\node\NodeInterface;

/**
 * Event subscriber for excluding pages from the cache.
 *
 * @package Drupal\cacheexclude.
 */
class CacheexcludeSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The current path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The page cache kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Constructs a new CacheexcludeSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path stack.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The page cache kill switch.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PathMatcherInterface $path_matcher, CurrentPathStack $current_path, AliasManagerInterface $alias_manager, RouteMatchInterface $route_match, KillSwitch $kill_switch) {
    $this->configFactory = $config_factory;
    $this->pathMatcher = $path_matcher;
    $this->currentPath = $current_path;
    $this->aliasManager = $alias_manager;
    $this->routeMatch = $route_match;
    $this->killSwitch = $kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::REQUEST][] = ['disableCacheForPage'];

    return $events;
  }

  /**
   * Subscriber Callback for the event.
   */
  public function disableCacheForPage(): void {
    // Path checking routine.
    if ($this->checkPath() === TRUE) {
      // Disable page cache temporarily.
      $this->killSwitch->trigger();
      return;
    }

    // Check if current node type is one we want to exclude from the cache.
    $node = $this->routeMatch->getParameter('node');
    if ($node instanceof NodeInterface) {
      $node_type = $node->getType();
    }

    $config = $this->configFactory->get('cacheexclude.settings');
    $node_types = $config->get('cacheexclude_node_types');

    if (!is_null($node_types)) {
      $node_types = array_filter($node_types);
      if (isset($node_type) && in_array($node_type, $node_types, TRUE)) {
        // Disable page cache temporarily.
        $this->killSwitch->trigger();
      }
    }
  }

  /**
   * Checks whether module configuration excludes the current path.
   *
   * @return bool
   *   TRUE if the path should not be cached.
   */
  private function checkPath() {
    // Get cacheexclude page configuration.
    $config = $this->configFactory->get('cacheexclude.settings');
    // Only trim if config exists.
    $pages = !is_null($config->get('cacheexclude_list')) ? trim($config->get('cacheexclude_list')) : NULL;

    // If the current page is one we want to exclude from the cache,
    // disable page cache temporarily.
    if (!is_null($pages)) {
      $current_path = $this->currentPath->getPath();
      $current_path_alias = $this->aliasManager->getAliasByPath($current_path);
      $path_matches = $this->pathMatcher->matchPath($current_path, $pages);
      $alias_path_matches = $this->pathMatcher->matchPath($current_path_alias, $pages);

      if ($path_matches || $alias_path_matches) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
